#!/bin/bash
echo user passed $withUser - password $withPassword
export ENV_USER=${withUser:-sae}
export ENV_PASSWORD=${withPassword:-$ENV_USER}
echo ${ENV_USER} --- ${ENV_PASSWORD}

echo "Setting Up Parent Containers...."
/usr/bin/containervncbrowsers-setup.sh

echo "Setting Up SmartCards...."
/usr/bin/containervncsmartcard-installSmartCard.sh $ENV_USER

echo "Finishing the Setting Up of SmartCards...."
/usr/bin/configUserNssdb $ENV_USER $ENV_PASSWORD

