FROM saearagon/ubuntuvncbrowsers:manifest-latest
MAINTAINER Servicio de Administracion Electronica
RUN apt-get update
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install pcscd pcsc-tools screen p11-kit libnss3 libnss3-tools libccid opensc pinentry-gtk2 bash supervisor
RUN dpkg --print-architecture
WORKDIR "/tmp"

COPY containervncsmartcard-installSmartCard.sh /usr/bin/containervncsmartcard-installSmartCard.sh
RUN chmod 777 -R /usr/bin/containervncsmartcard-installSmartCard.sh

COPY configUserNssdb /usr/bin/configUserNssdb
RUN chmod 777 -R /usr/bin/configUserNssdb
COPY runcontainer_vncsmartcard /usr/bin/runcontainer_vncsmartcard
RUN chmod 777 -R /usr/bin/runcontainer_vncsmartcard
COPY containervncsmartcard-setup.sh /usr/bin/containervncsmartcard-setup.sh
RUN chmod 777 -R /usr/bin/containervncsmartcard-setup.sh
COPY supervisord.conf /etc/supervisord.conf
#RUN ls -lisa /usr/bin/config*
RUN mkdir /run/sshd
ENTRYPOINT ["/bin/bash", "-c", "/usr/bin/runcontainer_vncsmartcard"]