#!/bin/bash
export username=$1
export archType=$(dpkg --print-architecture)
echo "ArchType:<$archType>"
if [ "amd64" = "$archType" ]; then

	FILE=/home/$username/smartcard_installed.txt
	if test -f "$FILE"; then
	    echo "$FILE exists."
	else
		cd /tmp
		echo "First FNMT Library for ubuntu 18.04"
		#wget https://www.sede.fnmt.gob.es/documents/10445900/10700711/libpkcs11-fnmtdnie_1.5.0_Ubuntu_18.04_LTS_18.10_64bits.deb
		#dpkg -i libpkcs11-fnmtdnie_1.5.0_Ubuntu_18.04_LTS_18.10_64bits.deb
		wget https://www.sede.fnmt.gob.es/documents/10445900/10816745/libpkcs11-fnmtdnie_1.6.1_Ubuntu_20.04_LTS_64bits.deb
		dpkg -i libpkcs11-fnmtdnie_1.6.1_Ubuntu_20.04_LTS_64bits.deb
		
		echo "Now Bit4Id drivers for ubuntu 18.04"
		wget http://cdn.bit4id.com/es/soporte/downloads/middleware/Bit4id_Middleware.zip
		unzip Bit4id_Middleware.zip
		cd Bit4id_Middleware_Linux
		dpkg -i libbit4xpki-bit4id-user-amd64.1.4.10-582.deb
	#	cp "Linux_ 4.0.0.5/linux64/libbit4ipki.so" /usr/lib/libbit4ipki.so
	#	cp "Linux_ 4.0.0.5/linux64/libbit4ipki.so" /usr/local/lib/libbit4ipki.so
	#	cp -r "Linux_ 4.0.0.5/linux64/pinman_64_bit" /usr/local/lib/pinman_64_bit
		echo "SmartCardInstalled" > $FILE
	fi
else
	echo "Architecture: $archType The fnmt and bit4id drivers only runs in AMD64 arch. Avoid install of drivers"
fi
